# Ancile Microsoft Application Experience Program

### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSAEP

This plugin disables telemetry reporting from the Microsoft Application Experience Program

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile